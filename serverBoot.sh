#!/bin/bash

##
## Startup script for Hystrix Server.
##
set -e

TO_RUN=client-*
ENV=
if [[ "x$1" == "xfatJar" || "x$2" == "xfatJar" || "x$3" == "xfatJar" ]]; then
    TO_RUN=container-*RELEASE
fi

if [[ "x$1" == "xprod" || "x$2" == "xprod" || "x$3" == "xprod" ]]; then
    ENV="--spring.profiles.active=production"
fi

CONFIG_APPLICATION=hystrix
CONFIG_APPLICATION_EXTENSION=jar
PATH_TO_EXECUTABLE=${CONFIG_APPLICATION}-dist # <-- no trailing slash here.
# jvm arguments added here
JVM_OPTS=""

if [[ "x$1" == "xdebug" || "x$2" == "xdebug" || "x$3" == "xdebug" ]]; then
    JVM_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5007"
fi

java ${JVM_OPTS} -jar ${PATH_TO_EXECUTABLE}/${CONFIG_APPLICATION}-${TO_RUN}.${CONFIG_APPLICATION_EXTENSION} ${ENV}
